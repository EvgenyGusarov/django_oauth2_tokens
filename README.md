# README #

Minimalistic Django app for creating and storing OAuth2 tokens for social networks.
The only requirements are Django>=1.8 and requests.

It is possible to maintain multiple OAuth clients for each social network.

Supported networks:

* vk.com
* facebook.com