from setuptools import setup

setup(
    name='oauth2_tokens',
    version='0.1',
    description='Minimalistic django app for creating and storing OAuth2 tokens for social networks',
    url='',
    author='Evgeny Gusarov',
    author_email='e.gusarov.dev@yandex.ru',
    license='MIT',
    packages=['oauth2_tokens'],
    install_requires=['Django>=1.8', 'requests', ],
    zip_safe=False
)
