# coding: utf-8
from __future__ import unicode_literals

from datetime import timedelta
from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone


class VkApp(models.Model):
    client_id = models.CharField(max_length=255, default='', validators=[RegexValidator('^\d+$')])
    client_secret = models.CharField(max_length=255, default='')
    redirect_uri = models.CharField(max_length=255, default='')
    name = models.CharField(max_length=255, default='')
    scope = models.TextField(blank=True, default='')
    created = models.DateTimeField(blank=True, null=True, auto_now_add=True)

    def __unicode__(self):
        return self.name

    PERMISSIONS = {
        'notify': 0,
        'friends': 1,
        'photos': 2,
        'audio': 3,
        'video': 4,
        'pages': 7,
        '+256': 8,
        'status': 10,
        'notes': 11,
        'messages': 12,
        'wall': 13,
        'ads': 15,
        'offline': 16,
        'docs': 17,
        'groups': 18,
        'notifications': 19,
        'stats': 20,
        'email': 22,
        'market': 27
    }

    @property
    def scope_mask(self):
        mask = 0

        for permission in self.scope.split(','):
            if permission in self.PERMISSIONS:
                mask += 1 << self.PERMISSIONS[permission]
            else:
                print('unknown permission ' + permission)

        return mask


class VkToken(models.Model):
    token = models.CharField(max_length=255, default='')
    created = models.DateTimeField(auto_now_add=True)
    oauth_user_id = models.CharField(max_length=255, validators=[RegexValidator('^\d+$')])
    app = models.ForeignKey('VkApp', null=True, blank=True, on_delete=models.CASCADE)
    is_valid = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = u'Vk tokens'

    def __unicode__(self):
        return self.token


class FbApp(models.Model):
    client_id = models.CharField(max_length=255, default='', validators=[RegexValidator('^\d+$')])
    client_secret = models.CharField(max_length=100)
    name = models.CharField(max_length=255, blank=True, null=True)
    scope = models.TextField(blank=True, null=True)
    redirect_uri = models.CharField(max_length=255, default='')
    created = models.DateTimeField(blank=True, null=True, auto_now_add=True)

    def __unicode__(self):
        return u'{}'.format(self.name)

    @property
    def app_token(self):
        return '{}|{}'.format(self.client_id, self.client_secret)


class FbToken(models.Model):
    TYPE_USER = 'user'
    TYPE_PAGE = 'page'

    TYPES = (
        (TYPE_USER, 'user'),
        (TYPE_PAGE, 'page'),
    )

    token = models.TextField(blank=True, default='')
    app = models.ForeignKey('FbApp', blank=True, null=True, on_delete=models.CASCADE)
    type = models.CharField(max_length=255, choices=TYPES, default=TYPE_USER)
    oauth_user_id = models.CharField(max_length=255, validators=[RegexValidator('^\d+$')])
    created = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    expires = models.IntegerField(blank=True, null=True)
    is_valid = models.BooleanField(blank=True, default=True)

    @property
    def is_expired(self):
        if not self.expires:
            return False

        elapsed = timezone.now() - self.created
        return elapsed > timedelta(seconds=self.expires)

    @property
    def expiration_date(self):
        if self.created and self.expires:
            return self.created + timedelta(seconds=self.expires)
        else:
            return None

    def __unicode__(self):
        width = 5
        return self.token[:width] + ' . . . ' + self.token[-width:] if len(self.token) > 2 * width else self.token
