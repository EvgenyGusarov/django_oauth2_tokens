# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin
from . import models


@admin.register(models.VkApp)
class VkAppAdmin(admin.ModelAdmin):
    list_display = ('name', 'client_id', 'redirect_uri', )


@admin.register(models.VkToken)
class VkTokenAdmin(admin.ModelAdmin):
    list_display = ('token', 'oauth_user_id', 'created', 'app', 'is_valid', )
    search_fields = ('token', )
    list_filter = ('app', )
    list_select_related = ('app',)


@admin.register(models.FbApp)
class FbAppAdmin(admin.ModelAdmin):
    list_display = ('name', 'client_id', 'scope', 'created', 'redirect_uri', )


@admin.register(models.FbToken)
class FbTokenAdmin(admin.ModelAdmin):
    list_display = ('short_token', 'app', 'type', 'oauth_user_id', 'created', 'expires', 'get_expiration_date',
                    'is_valid', )
    list_filter = ('app', 'type', 'is_valid', )
    list_select_related = ('app', )

    def short_token(self, obj):
        return unicode(obj)
    short_token.short_description = 'token'
    short_token.admin_order_field = 'token'

    def get_expiration_date(self, obj):
        return obj.expiration_date
    get_expiration_date.short_description = 'expiration date'
    get_expiration_date.admin_order_field = 'created'
