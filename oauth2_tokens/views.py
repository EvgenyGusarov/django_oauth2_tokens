# coding: utf-8
from __future__ import unicode_literals

import urllib
import requests
import json
from django.conf import settings
from django.http.response import HttpResponseRedirect, JsonResponse, HttpResponse
from django.views.generic import View
from .models import VkApp, VkToken, FbApp, FbToken


DEFAULT_VK_API_VERSION = '5.69'


class VkAuthView(View):
    url = 'https://oauth.vk.com/authorize'

    def get(self, request, app_id):
        app = VkApp.objects.get(client_id=app_id)
        group_id = request.GET.get('group_id')

        redirect_params = {'app_id': app_id}
        if group_id:
            redirect_params['group_id'] = group_id

        params = {
            'client_id': str(app.client_id),
            'scope': app.scope_mask,
            'redirect_uri': app.redirect_uri + '?' + urllib.urlencode(redirect_params),
            'response_type': 'code',
            'v': getattr(settings, 'VK_API_VERSION', DEFAULT_VK_API_VERSION),
        }

        return HttpResponseRedirect(
            self.url + '?' + urllib.urlencode(params)
        )


class VkTokenView(View):
    url = 'https://oauth.vk.com/access_token'

    def get(self, request):
        code = request.GET.get('code')
        app_id = request.GET.get('app_id')
        app = VkApp.objects.get(client_id=app_id)

        # redirect_uri must be the same as in authorize method
        redirect_params = {'app_id': app_id}

        params = {
            'code': code,
            'client_id': str(app.client_id),
            'client_secret': app.client_secret,
            'redirect_uri': app.redirect_uri + '?' + urllib.urlencode(redirect_params),
        }
        r = requests.get(self.url + '?' + urllib.urlencode(params))
        content = json.loads(r.content)

        if 'user_id' not in content:
            return JsonResponse(content)

        user_id = content['user_id']

        token = VkToken(oauth_user_id=user_id, app_id=app.id, token=content['access_token'])
        token.save()

        return JsonResponse(content)


DEFAULT_FB_API_VERSION = 'v2.11'


class FbAuthView(View):
    """
    https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow
    """
    url = 'https://www.facebook.com/{version}/dialog/oauth'

    def get(self, request, app_id):
        app = FbApp.objects.get(client_id=app_id)

        params = {
            'client_id': app.client_id,
            'scope': app.scope,
            'response_type': 'code',
            'redirect_uri': app.redirect_uri,
            'state': app.client_id,
        }

        version = getattr(settings, 'FB_API_VERSION', DEFAULT_FB_API_VERSION)
        url = self.url.format(version=version)

        return HttpResponseRedirect(
            url + '?' + urllib.urlencode(params)
        )


class FbTokenView(View):
    url = 'https://graph.facebook.com/{version}/oauth/access_token'

    def get(self, request):
        code = request.GET.get('code', '')
        client_id = request.GET.get('state', '')
        app = FbApp.objects.get(client_id=client_id)

        params = {
            'code': code,
            'client_id': app.client_id,
            'client_secret': app.client_secret,
            'redirect_uri': app.redirect_uri,
            'grant_type': 'authorization_code'
        }
        version = getattr(settings, 'FB_API_VERSION', DEFAULT_FB_API_VERSION)
        url = self.url.format(version=version)
        res = requests.get(url + '?' + urllib.urlencode(params))
        print('content {}'.format(res.content))

        try:
            content = json.loads(res.content)
            if 'error' in content:
                return JsonResponse(content)
        except ValueError:
            return HttpResponse(res.content)

        print('token', content['access_token'])
        fb_token = FbToken(token=content['access_token'], expires=content.get('expires_in', 0), app=app)
        print('app.app_token', app.app_token)

        token_info = inspect_fb_token(fb_token.token, app.app_token)
        print('token_info', token_info)
        fb_token.oauth_user_id = token_info['user_id']
        fb_token.save()

        return JsonResponse(content)


def inspect_fb_token(input_token, app_token):
    url = 'https://graph.facebook.com/debug_token'
    params = {
        'input_token': input_token,
        'access_token': app_token,
    }
    res = requests.get(url + '?' + urllib.urlencode(params))
    return json.loads(res.content)['data']
