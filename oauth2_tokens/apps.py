# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class OAuthTokensConfig(AppConfig):
    name = 'oauth2_tokens'
    verbose_name = 'OAuth2 tokens'
