# coding: utf-8
from __future__ import unicode_literals

from django.conf.urls import url
from .views import VkAuthView, VkTokenView, FbAuthView, FbTokenView


urlpatterns = [
    url(r'^vk/auth/(?P<app_id>\d+)/', VkAuthView.as_view(), name='vk_auth'),
    url(r'^vk/token/', VkTokenView.as_view(), name='vk_token'),
    url(r'^fb/auth/(?P<app_id>\d+)/', FbAuthView.as_view(), name='fb_auth'),
    url(r'^fb/token/', FbTokenView.as_view(), name='fb_token'),
]
